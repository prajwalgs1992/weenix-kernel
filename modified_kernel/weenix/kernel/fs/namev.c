/******************************************************************************/
/* Important Summer 2017 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "kernel.h"
#include "globals.h"
#include "types.h"
#include "errno.h"

#include "util/string.h"
#include "util/printf.h"
#include "util/debug.h"

#include "fs/dirent.h"
#include "fs/fcntl.h"
#include "fs/stat.h"
#include "fs/vfs.h"
#include "fs/vnode.h"

/* This takes a base 'dir', a 'name', its 'len', and a result vnode.
 * Most of the work should be done by the vnode's implementation
 * specific lookup() function.
 *
 * If dir has no lookup(), return -ENOTDIR.
 *
 * Note: returns with the vnode refcount on *result incremented.
 */
int
lookup(vnode_t *dir, const char *name, size_t len, vnode_t **result)
{
        /*
	 NOT_YET_IMPLEMENTED("VFS: lookup");
         return 0;
	*/
	dbg(DBG_CORE, "Entered lookup\n");
    	KASSERT(NULL != dir);
    	KASSERT(NULL != name);
    	KASSERT(NULL != result);
    	dbg(DBG_PRINT, "(GRADING2A 2.a)\n");
	

	if((dir->vn_ops->lookup == NULL) || !(S_ISDIR(dir->vn_mode)))
	{
		dbg(DBG_PRINT,"(GRADING2B)\n");
		vput(dir);
		return -ENOTDIR;
	}
	if(len!=0)
	{
		dbg(DBG_PRINT,"(GRADING2B)\n");
		int res = dir->vn_ops->lookup(dir, name, len, result);
		dbg(DBG_CORE, "res = %d\n", res);
	
		return res;
	}
	else if(len ==0)
	{
		dbg(DBG_PRINT,"(GRADING2C 1)\n");
		*result=vget(dir->vn_fs, dir->vn_vno);
		return 0;
	}
	return 0;
}


/* When successful this function returns data in the following "out"-arguments:
 *  o res_vnode: the vnode of the parent directory of "name"
 *  o name: the `basename' (the element of the pathname)
 *  o namelen: the length of the basename
 *
 * For example: dir_namev("/s5fs/bin/ls", &namelen, &name, NULL,
 * &res_vnode) would put 2 in namelen, "ls" in name, and a pointer to the
 * vnode corresponding to "/s5fs/bin" in res_vnode.
 *
 * The "base" argument defines where we start resolving the path from:
 * A base value of NULL means to use the process's current working directory,
 * curproc->p_cwd.  If pathname[0] == '/', ignore base and start with
 * vfs_root_vn.  dir_namev() should call lookup() to take care of resolving each
 * piece of the pathname.
 *
 * Note: A successful call to this causes vnode refcount on *res_vnode to
 * be incremented.
 */
int
dir_namev(const char *pathname, size_t *namelen, const char **name,
          vnode_t *base, vnode_t **res_vnode)
{
        /*
	 NOT_YET_IMPLEMENTED("VFS: dir_namev");
         return 0;
	*/

	KASSERT(NULL != pathname);
        KASSERT(NULL != namelen);
        KASSERT(NULL != name);
        KASSERT(NULL != res_vnode);
	dbg(DBG_PRINT,"(GRADING2A 2.b)\n");

	dbg(DBG_CORE, "Entered dir_namev\n");
	dbg(DBG_CORE, "pathname = %s\n", pathname);

	
    	vnode_t *node;
	if (pathname == NULL)
	{
		dbg(DBG_PRINT,"(GRADING2A 2.b)\n");
	        return -EINVAL;
	}
	
	if (pathname[0] == '/')
	{
		dbg(DBG_PRINT,"(GRADING2B)\n");
	        node = vfs_root_vn;
  	} 
	else if (base == NULL)
	{
		dbg(DBG_PRINT,"(GRADING2B)\n");
        	node = curproc->p_cwd;
    	} 
	else
	{
		dbg(DBG_PRINT,"(GRADING2B)\n");
        	node = base;
    	}

   	vref(node);

	vnode_t *node2 = NULL;
	int slashlen = 0;
    	int i = 0;
    	int res = 1;
    	int len;
    	int errcode = 0;

	dbg(DBG_PRINT,"(GRADING2B)\n");
    	while (res >= 0 && pathname[i] != '\0')
	{
		
	        if (node2 != NULL)
		{
			dbg(DBG_PRINT,"(GRADING2B)\n");
	            vput(node2);
	        }

	        node2 = node;

	        slashlen = i;

		dbg(DBG_PRINT,"(GRADING2B)\n");
	        while (pathname[i] != '/' && pathname[i] != '\0')
		{
			
	            	i++;
	        }
		len = i - slashlen;

        	if (i - slashlen > NAME_LEN)
		{
			dbg(DBG_PRINT,"(GRADING2B)\n");
	            	errcode = -ENAMETOOLONG;
	            	break;
	        }

		KASSERT(NULL !=node2);
		dbg(DBG_PRINT,"(GRADING2A 2.b)\n");	
		
        	res = lookup(node2, (pathname + slashlen), i - slashlen, &node);
		
		if(!S_ISDIR(node->vn_mode)&&(pathname[i+1]=='\0')&&(pathname[i]=='/'))
		{
			dbg(DBG_PRINT,"(GRADING2B)\n");
			errcode= -ENOTDIR;
			break;
		}
		
		
       		if (res == -ENOTDIR)
		{
			dbg(DBG_PRINT,"(GRADING2B)\n");
	        	errcode = -ENOTDIR;
	           	break;
	        }
		
  		dbg(DBG_PRINT,"(GRADING2B)\n");
        	while (pathname[i] == '/')
		{
	            	i++;
	        }
	}
	

	if((errcode==-ENOTDIR)&&(pathname[strlen(pathname)-1]=='/'))
	{
		dbg(DBG_PRINT,"(GRADING2B)\n");
		vput(node2);
		vput(node);
        	return -ENOTDIR;
	}
	else if (res == 0)
	{
		dbg(DBG_PRINT,"(GRADING2B)\n");
        	vput(node);
    	}

    	if (res < 0 && res != -ENOENT)
	{
	 	dbg(DBG_PRINT,"(GRADING2B)\n");       
	        return res;
    	} 
	else if (pathname[i] != '\0')
	{
		dbg(DBG_PRINT,"(GRADING2B)\n");
	        vput(node2);
	        return -ENOENT;
    	}
	else if (errcode != 0)
	{
		dbg(DBG_PRINT,"(GRADING2B)\n");
     		vput(node2);

        	return errcode;
 	} 
	
	*namelen = len;
	*name = (pathname + slashlen);

    	*res_vnode = node2;
	
	return 0;
}

/* This returns in res_vnode the vnode requested by the other parameters.
 * It makes use of dir_namev and lookup to find the specified vnode (if it
 * exists).  flag is right out of the parameters to open(2); see
 * <weenix/fcntl.h>.  If the O_CREAT flag is specified, and the file does
 * not exist call create() in the parent directory vnode.
 *
 * Note: Increments vnode refcount on *res_vnode.
 */
int
open_namev(const char *pathname, int flag, vnode_t **res_vnode, vnode_t *base)
{
	/*
         NOT_YET_IMPLEMENTED("VFS: open_namev");
         return 0;
	*/
	
	dbg(DBG_CORE, "Entered open_namev\n");
	size_t namelen;
	const char* name;
	int ret;
	dbg(DBG_CORE, "pathname = %s\n", pathname);
	vnode_t* node = NULL;
	
	int res = dir_namev(pathname, &namelen, &name, base, &node);
	
	if(res==0)
	{
		dbg(DBG_PRINT,"(GRADING2B)\n");
		res = lookup(node, name, namelen, res_vnode);
	
		 ret = res;
	
		if ((res == -ENOENT) && (flag & O_CREAT))
		{
	      		KASSERT(NULL != node->vn_ops->create);
			dbg (DBG_PRINT, "(GRADING2A 2.c)\n");
			dbg(DBG_PRINT, "(GRADING2B)\n");

           		ret = node->vn_ops->create(node, name, namelen, res_vnode);
			if (ret<0) {
				dbg(DBG_PRINT, "(GRADING2C 1)\n");
				vput(node);
				return ret;
    			}
		}
		else if ((res == -ENOENT) && !(flag & O_CREAT))
		{
			dbg(DBG_PRINT,"(GRADING2B)\n");
			vput(node);
			return -ENOENT;
		}	
		else if (res < 0)
		{
			dbg(DBG_PRINT,"(GRADING2B)\n");
        		vput(node);
			return res;
    		} 
		else if ((*res_vnode)->vn_ops->mkdir != NULL && ((flag & O_WRONLY) || (flag & O_RDWR)))
		{     
			dbg(DBG_PRINT,"(GRADING2B)\n");
       			ret = -EISDIR;
       			vput(*res_vnode);
		        *res_vnode = NULL;
			vput(node);
	
			return ret;
	    	}
		dbg(DBG_PRINT,"(GRADING2B)\n");
    		vput(node);
	
		return ret;
	}
	else
	{
		dbg(DBG_PRINT,"(GRADING2B)\n");
		return res;
	}
}

#ifdef __GETCWD__
/* Finds the name of 'entry' in the directory 'dir'. The name is writen
 * to the given buffer. On success 0 is returned. If 'dir' does not
 * contain 'entry' then -ENOENT is returned. If the given buffer cannot
 * hold the result then it is filled with as many characters as possible
 * and a null terminator, -ERANGE is returned.
 *
 * Files can be uniquely identified within a file system by their
 * inode numbers. */
int
lookup_name(vnode_t *dir, vnode_t *entry, char *buf, size_t size)
{
        NOT_YET_IMPLEMENTED("GETCWD: lookup_name");
        return -ENOENT;
}


/* Used to find the absolute path of the directory 'dir'. Since
 * directories cannot have more than one link there is always
 * a unique solution. The path is writen to the given buffer.
 * On success 0 is returned. On error this function returns a
 * negative error code. See the man page for getcwd(3) for
 * possible errors. Even if an error code is returned the buffer
 * will be filled with a valid string which has some partial
 * information about the wanted path. */
ssize_t
lookup_dirpath(vnode_t *dir, char *buf, size_t osize)
{
        NOT_YET_IMPLEMENTED("GETCWD: lookup_dirpath");

        return -ENOENT;
}
#endif /* __GETCWD__ */
