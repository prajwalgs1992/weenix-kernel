/******************************************************************************/
/* Important Summer 2017 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "types.h"
#include "globals.h"
#include "errno.h"

#include "util/debug.h"
#include "util/string.h"

#include "proc/proc.h"
#include "proc/kthread.h"

#include "mm/mm.h"
#include "mm/mman.h"
#include "mm/page.h"
#include "mm/pframe.h"
#include "mm/mmobj.h"
#include "mm/pagetable.h"
#include "mm/tlb.h"

#include "fs/file.h"
#include "fs/vnode.h"

#include "vm/shadow.h"
#include "vm/vmmap.h"

#include "api/exec.h"

#include "main/interrupt.h"

/* Pushes the appropriate things onto the kernel stack of a newly forked thread
 * so that it can begin execution in userland_entry.
 * regs: registers the new thread should have on execution
 * kstack: location of the new thread's kernel stack
 * Returns the new stack pointer on success. */
static uint32_t
fork_setup_stack(const regs_t *regs, void *kstack)
{
        /* Pointer argument and dummy return address, and userland dummy return
         * address */
        uint32_t esp = ((uint32_t) kstack) + DEFAULT_STACK_SIZE - (sizeof(regs_t) + 12);
        *(void **)(esp + 4) = (void *)(esp + 8); /* Set the argument to point to location of struct on stack */
        memcpy((void *)(esp + 8), regs, sizeof(regs_t)); /* Copy over struct */
        return esp;
}


/*
 * The implementation of fork(2). Once this works,
 * you're practically home free. This is what the
 * entirety of Weenix has been leading up to.
 * Go forth and conquer.
 */
int
do_fork(struct regs *regs)
{
	/*
        NOT_YET_IMPLEMENTED("VM: do_fork");
        return 0;
	*/
	KASSERT(regs != NULL);
        KASSERT(curproc != NULL);
        KASSERT(curproc->p_state == PROC_RUNNING);
	dbg(DBG_PRINT,"(GRADING3A 7.a)\n");


	proc_t* child_proc = proc_create("child");
	child_proc->p_vmmap = vmmap_clone(curproc->p_vmmap);	
	/* never reached*/
	/*if(child_proc->p_vmmap == NULL)
	{
		proc_kill(child_proc, EBADF);
		dbg(DBG_PRINT,"(GRADING3D 4) return error from fork\n");
		return -1;
	}
	*/
	KASSERT(child_proc->p_state == PROC_RUNNING);
        KASSERT(child_proc->p_pagedir != NULL);
	dbg(DBG_PRINT,"(GRADING3A 7.a)\n");
	
	vmarea_t* parent_vma;
	
	list_iterate_begin(&(curproc->p_vmmap->vmm_list), parent_vma, vmarea_t, vma_plink)
	{
		struct mmobj *cur_mmobj = parent_vma->vma_obj;    
               
		if((parent_vma->vma_flags & MAP_PRIVATE) == MAP_PRIVATE)
		{
			vmarea_t *new_vma = vmmap_lookup(child_proc->p_vmmap, parent_vma->vma_start);
                    	struct mmobj *new_vma_shadow = shadow_create();
                    	new_vma_shadow->mmo_shadowed = cur_mmobj;
                    	new_vma_shadow->mmo_un.mmo_bottom_obj = mmobj_bottom_obj(cur_mmobj); 
                   	list_insert_tail(&new_vma_shadow->mmo_un.mmo_bottom_obj->mmo_un.mmo_vmas, &new_vma->vma_olink);
                    	new_vma->vma_obj = new_vma_shadow;

                    	struct mmobj *cvma_shadow = shadow_create();
                    	cvma_shadow->mmo_shadowed = cur_mmobj;
                    	cvma_shadow->mmo_un.mmo_bottom_obj = mmobj_bottom_obj(cur_mmobj); 
                    	parent_vma->vma_obj = cvma_shadow;

                    	parent_vma->vma_obj->mmo_shadowed->mmo_ops->ref(parent_vma->vma_obj->mmo_shadowed);
			dbg(DBG_PRINT, "(GRADING3D 4) if parent_vma->vma_flags = MAP_PRIVATE\n");
		}
		else if( (parent_vma->vma_flags & MAP_SHARED) == MAP_SHARED)
		{
			vmarea_t *new_vma = vmmap_lookup(child_proc->p_vmmap, parent_vma->vma_start);
                        new_vma->vma_obj = cur_mmobj;
                        list_insert_head(&(new_vma->vma_obj->mmo_un.mmo_vmas), &new_vma->vma_olink);
                        new_vma->vma_obj->mmo_ops->ref(new_vma->vma_obj);
			dbg(DBG_PRINT, "(GRADING3D 3) if parent_vma->vma_flags = MAP_SHARED\n");
		}
	}list_iterate_end();
	
	pt_unmap_range(curproc->p_pagedir, USER_MEM_LOW, USER_MEM_HIGH);
	
	tlb_flush_all();
	
	kthread_t* child_thr = kthread_clone(curthr);
	
	child_thr->kt_proc = child_proc;
	regs->r_eax=0;
	
	(child_thr->kt_ctx).c_eip = (uint32_t)userland_entry;
	(child_thr->kt_ctx).c_pdptr = child_proc->p_pagedir;
	KASSERT(child_thr->kt_kstack != NULL);
	dbg(DBG_PRINT,"(GRADING3A 7.a)\n");	
	
	(child_thr->kt_ctx).c_esp = fork_setup_stack(regs, child_thr->kt_kstack);
	
	list_insert_tail(&child_proc->p_threads,&(child_thr->kt_plink));
	
	memcpy(child_proc->p_files, curproc->p_files, sizeof(curproc->p_files));
	
	int i;
	
	for (i = 0; i < NFILES; i++)
	{
		if (curproc->p_files[i] != 0)
		{
			fref(curproc->p_files[i]);
			dbg(DBG_PRINT, "(GRADING3D 4) fref for child process\n");
		}
	}
	
	
	child_proc->p_brk = curproc->p_brk;
	child_proc->p_start_brk = curproc->p_start_brk;
	
	sched_make_runnable(child_thr);
	dbg(DBG_PRINT,"(GRADING3D 4) return success from fork\n");
	return child_proc->p_pid;
}
