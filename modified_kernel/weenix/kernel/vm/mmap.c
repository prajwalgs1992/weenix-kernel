/******************************************************************************/
/* Important Summer 2017 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "globals.h"
#include "errno.h"
#include "types.h"

#include "mm/mm.h"
#include "mm/tlb.h"
#include "mm/mman.h"
#include "mm/page.h"

#include "proc/proc.h"

#include "util/string.h"
#include "util/debug.h"

#include "fs/vnode.h"
#include "fs/vfs.h"
#include "fs/file.h"

#include "vm/vmmap.h"
#include "vm/mmap.h"

/*
 * This function implements the mmap(2) syscall, but only
 * supports the MAP_SHARED, MAP_PRIVATE, MAP_FIXED, and
 * MAP_ANON flags.
 *
 * Add a mapping to the current process's address space.
 * You need to do some error checking; see the ERRORS section
 * of the manpage for the problems you should anticipate.
 * After error checking most of the work of this function is
 * done by vmmap_map(), but remember to clear the TLB.
 */
int
do_mmap(void *addr, size_t len, int prot, int flags,
        int fd, off_t off, void **ret)
{
        /*NOT_YET_IMPLEMENTED("VM: do_mmap");
        return -1;*/
	dbg(DBG_CORE,"Enter do_mmap()\n");

		/* check for errors */
	if( (addr!= NULL) || ((flags & MAP_FIXED) == MAP_FIXED) )
	{
		if((uint32_t)addr < USER_MEM_LOW)
		{
			dbg(DBG_PRINT,"(GRADING3D 1) Enter addr < USER_MEM_LOW do_mmap()\n");	
			return -EINVAL;
		}
		/* never reached */
		/*if( ((uint32_t)addr + len) > USER_MEM_HIGH )
		{
			dbg(DBG_PRINT,"(GRADING3D 1) Enter (uint32_t)addr + len) > USER_MEM_HIGH do_mmap()\n");	
			return -EINVAL;
		}*/
		dbg(DBG_PRINT,"(GRADING3D 1) return when (addr!= NULL) || ((flags & MAP_FIXED) == MAP_FIXED) do_mmap()\n");
	}
	
	if( ((uint32_t)addr >= (uint32_t)addr+len) || !PAGE_ALIGNED(addr) || !PAGE_ALIGNED(off) )
	{
		dbg(DBG_PRINT,"(GRADING3D 1) Enter if (uint32_t)addr >= (uint32_t)addr+len) || !PAGE_ALIGNED(addr) || !PAGE_ALIGNED(off) do_mmap()\n");		
		return -1;
	}
	
	if( !( (flags&MAP_PRIVATE) || (flags&MAP_SHARED) ) || ( (flags&MAP_PRIVATE) && (flags&MAP_SHARED) ) )
	{
		dbg(DBG_PRINT,"(GRADING3D 1) Enter if !( (flags&MAP_PRIVATE) || (flags&MAP_SHARED) ) || ( (flags&MAP_PRIVATE) && (flags&MAP_SHARED) ) do_mmap()\n");
        	return -EINVAL;
	}
	
	vnode_t* vnode = NULL;
	if( !(flags&MAP_ANON) )
	{
		/*dbg(DBG_PRINT,"(GRADING3B 1) Enter !(flags&MAP_ANON) in do_mmap()\n");*/
		file_t* file_ptr = fget(fd);	
		if(file_ptr == NULL)
		{
			dbg(DBG_PRINT,"(GRADING3D 1) Enter file_ptr == null do_mmap()\n");
			return -EBADF;
		}
		if( (prot&PROT_READ) && ((file_ptr->f_mode&FMODE_READ) != FMODE_READ) )
		{
			fput(file_ptr);
			dbg(DBG_PRINT,"(GRADING3D 1) Enter (prot&PROT_READ) && ((file_ptr->f_mode&FMODE_READ) != FMODE_READ) do_mmap()\n");
			return -EACCES;
		}
		if( (flags&MAP_SHARED) && (prot&PROT_WRITE) && !((file_ptr->f_mode&FMODE_READ) == FMODE_READ && (file_ptr->f_mode&FMODE_WRITE)))
		{
			fput(file_ptr);
			dbg(DBG_PRINT,"(GRADING3D 1) (flags&MAP_SHARED) && (prot&PROT_WRITE) && !((file_ptr->f_mode&FMODE_READ) == FMODE_READ && (file_ptr->f_mode&FMODE_WRITE)) do_mmap()\n");
            		return -EACCES;
        	}
		vnode = file_ptr->f_vnode;
		vref(vnode);
		fput(file_ptr);
		dbg(DBG_PRINT,"(GRADING3D 1) coming out of if !(flags&MAP_ANON) in do_mmap()\n");
	}
	if(vnode != NULL)
	{
		vput(vnode);
		dbg(DBG_PRINT,"(GRADING3D 2) decrement vnode in do_mmap()\n");
	}
	
	uint32_t lopage = ADDR_TO_PN(addr);
	uint32_t npages = ((len-1) >> PAGE_SHIFT) + 1;
	
	vmarea_t* vma;
	
	int i = vmmap_map(curproc->p_vmmap, vnode, lopage, npages, prot, flags, off, VMMAP_DIR_HILO, &vma);
	
	if(i)
	{
		dbg(DBG_PRINT,"(GRADING3D 2) return failure if vmmap_map is failure in do_mmap()\n");
		return -EINVAL;
	}
	else
	{
		*ret = PN_TO_ADDR(vma->vma_start);
		tlb_flush_range((uint32_t)*ret, npages);
		dbg(DBG_PRINT,"(GRADING3D 2) if vmmap_map is success in do_mmap()\n");
	}
	KASSERT(NULL != curproc->p_pagedir);
	dbg(DBG_PRINT,"(GRADING3A 2.a)\n");
	/*dbg(DBG_PRINT,"(GRADING3D 2) return success from do_mmap()\n");*/
	return 0;
}


/*
 * This function implements the munmap(2) syscall.
 *
 * As with do_mmap() it should perform the required error checking,
 * before calling upon vmmap_remove() to do most of the work.
 * Remember to clear the TLB.
 */
int
do_munmap(void *addr, size_t len)
{
        /*NOT_YET_IMPLEMENTED("VM: do_munmap");
        return -1;
        */

		/*dbg(DBG_PRINT,"(GRADING3D 2) do_munmap is entered from vfstest/memtest\n");*/
		if((int)len == 0) {
			dbg(DBG_PRINT,"(GRADING3D 2) when len ==0\n");
			return -EINVAL;
		}
		
		if((uint32_t)addr >= (uint32_t)addr + len)
		{
			dbg(DBG_PRINT,"(GRADING3D 2) when (uint32_t)addr >= (uint32_t)addr + len\n");
			return -EINVAL;
		}

		uint32_t start_addr = (uint32_t)addr;
		uint32_t end_addr = start_addr + len;

		
		if(start_addr < USER_MEM_LOW || end_addr > USER_MEM_HIGH) {
			dbg(DBG_PRINT,"(GRADING3D 2) when (uint32_t)addr >= (uint32_t)addr + len\n");
			return -EINVAL;
		}
		
		int vmr_ret = vmmap_remove(curproc->p_vmmap, ADDR_TO_PN(addr), (((len-1) >> PAGE_SHIFT) +1) );
		
		pt_unmap_range( curproc->p_pagedir, (uint32_t)addr, (uint32_t)addr + (uintptr_t)( (((len-1) >> PAGE_SHIFT) +1)*PAGE_SIZE ) );
		
		tlb_flush_range( (uint32_t)addr, (((len-1) >> PAGE_SHIFT) + 1) );
		dbg(DBG_PRINT,"(GRADING3D 2) return success from do_munmap()\n");
		return 0;
}

