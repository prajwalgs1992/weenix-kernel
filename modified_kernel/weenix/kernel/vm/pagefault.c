/******************************************************************************/
/* Important Summer 2017 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "types.h"
#include "globals.h"
#include "kernel.h"
#include "errno.h"

#include "util/debug.h"

#include "proc/proc.h"

#include "mm/mm.h"
#include "mm/mman.h"
#include "mm/page.h"
#include "mm/mmobj.h"
#include "mm/pframe.h"
#include "mm/pagetable.h"

#include "vm/pagefault.h"
#include "vm/vmmap.h"

/*
 * This gets called by _pt_fault_handler in mm/pagetable.c The
 * calling function has already done a lot of error checking for
 * us. In particular it has checked that we are not page faulting
 * while in kernel mode. Make sure you understand why an
 * unexpected page fault in kernel mode is bad in Weenix. You
 * should probably read the _pt_fault_handler function to get a
 * sense of what it is doing.
 *
 * Before you can do anything you need to find the vmarea that
 * contains the address that was faulted on. Make sure to check
 * the permissions on the area to see if the process has
 * permission to do [cause]. If either of these checks does not
 * pass kill the offending process, setting its exit status to
 * EFAULT (normally we would send the SIGSEGV signal, however
 * Weenix does not support signals).
 *
 * Now it is time to find the correct page (don't forget
 * about shadow objects, especially copy-on-write magic!). Make
 * sure that if the user writes to the page it will be handled
 * correctly.
 *
 * Finally call pt_map to have the new mapping placed into the
 * appropriate page table.
 *
 * @param vaddr the address that was accessed to cause the fault
 *
 * @param cause this is the type of operation on the memory
 *              address which caused the fault, possible values
 *              can be found in pagefault.h
 */
void
handle_pagefault(uintptr_t vaddr, uint32_t cause)
{
	/*
        NOT_YET_IMPLEMENTED("VM: handle_pagefault");
	*/
	
	vmarea_t* vma = vmmap_lookup(curproc->p_vmmap, ADDR_TO_PN(vaddr));
	
	if(vma == NULL)
	{
		dbg(DBG_PRINT,"(GRADING3D 2) when vmmap_lookup returns NULL called from memtest\n");
		do_exit(EFAULT);
		return;
	}
	/* never reached*/
	/*else if(cause & FAULT_PRESENT)
	{
		if(!(vma->vma_prot & PROT_READ))
		{
			dbg(DBG_PRINT,"(GRADING3D 2) cause&FAULT_PRESENT and !vma_prot&PROT_READ to exit\n");
			do_exit(EFAULT);
			return;
		}
	}*/
	else if(cause & FAULT_WRITE)
	{
		if(!(vma->vma_prot & PROT_WRITE))
		{
			dbg(DBG_PRINT,"(GRADING3D 2) cause&FAULT_WRITE and !vma_prot&PROT_WRITE to exit\n");
			do_exit(EFAULT);
			return;
		}
	}
	/*never reached*/
	/*else if(cause & FAULT_RESERVED)
	{
		if(!(vma->vma_prot & PROT_NONE))
		{
			dbg(DBG_PRINT,"(GRADING3D 2) cause&FAULT_RESERVED and !vma_prot&PROT_NONE to exit\n");
			do_exit(EFAULT);
			return;
		}
	}
	else if(cause & FAULT_EXEC)
	{
		if(!(vma->vma_prot & PROT_EXEC))
		{
			dbg(DBG_PRINT,"(GRADING3D 2) cause&FAULT_EXEC and !vma_prot&PROT_EXEC to exit\n");
			do_exit(EFAULT);
			return;
		}
	}*/

	else if(!((cause & FAULT_WRITE) || (cause & FAULT_EXEC))
            && !(vma->vma_prot & PROT_READ))
	{
			dbg(DBG_PRINT,"(GRADING3D 2) cause&FAULT_WRITE || EXEC and !vma_prot&PROT_READ to exit\n");
			do_exit(EFAULT);
			return;
	}	
	/* never reached*/
	/*else
	if (vma->vma_prot & PROT_NONE)
	{
			dbg(DBG_PRINT,"(GRADING3D 2) when vma_prot&PROT_NONE to exit\n");
			do_exit(EFAULT);
			return;
	}*/
		
	pframe_t* pf = NULL;
	
	int forwrite = 0;
	
	if(cause & FAULT_WRITE)
	{
		forwrite = 1;
		dbg(DBG_PRINT,"(GRADING3D 2) to set forwrite when cause & FAULT_WRITE\n");
	}

	int res = pframe_lookup(vma->vma_obj, (ADDR_TO_PN(vaddr)-vma->vma_start+vma->vma_off), forwrite, &pf);
	
	
	if(res < 0)
	{
		dbg(DBG_PRINT,"(GRADING3D 2) when lookup is failure in handle_pagefault\n");
		do_exit(EFAULT);
		return;
	}
	KASSERT(pf);
	KASSERT(pf->pf_addr);
	dbg(DBG_PRINT,"(GRADING3A 5.a)\n");

	if (cause & FAULT_WRITE)
	{
		pframe_pin(pf);
		int dirty_res = pframe_dirty(pf);
		pframe_unpin(pf);
		/* never reached*/
		/*if (dirty_res < 0)
		{
			dbg(DBG_PRINT,"(GRADING3D 2) when cause&FAULT_WRITE and pframe_dirty returns false to exit\n");
			do_exit(EFAULT);
			return;
		}*/
		dbg(DBG_PRINT,"(GRADING3D 2) when cause&FAULT_WRITE\n");
	}
	
	uint32_t pdflags = PD_PRESENT | PD_USER;

    	uint32_t ptflags = PT_PRESENT | PT_USER;

    	if (cause & FAULT_WRITE)
	{
        	pdflags |= PD_WRITE;
        	ptflags |= PT_WRITE;
		dbg(DBG_PRINT,"(GRADING3D 2) to set pdflags and ptflags to exit\n");
    	}


	pt_map(curproc->p_pagedir, (uintptr_t) PAGE_ALIGN_DOWN(vaddr), pt_virt_to_phys((uintptr_t) pf->pf_addr), pdflags, ptflags);
	dbg(DBG_PRINT,"(GRADING3D 1) exit from handle_pagefault\n");	
}
