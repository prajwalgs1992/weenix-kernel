/******************************************************************************/
/* Important Summer 2017 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "globals.h"
#include "errno.h"

#include "util/string.h"
#include "util/debug.h"

#include "mm/mmobj.h"
#include "mm/pframe.h"
#include "mm/mm.h"
#include "mm/page.h"
#include "mm/slab.h"
#include "mm/tlb.h"

#include "vm/vmmap.h"
#include "vm/shadow.h"
#include "vm/shadowd.h"

#define SHADOW_SINGLETON_THRESHOLD 5

int shadow_count = 0; /* for debugging/verification purposes */
#ifdef __SHADOWD__
/*
 * number of shadow objects with a single parent, that is another shadow
 * object in the shadow objects tree(singletons)
 */
static int shadow_singleton_count = 0;
#endif

static slab_allocator_t *shadow_allocator;

static void shadow_ref(mmobj_t *o);
static void shadow_put(mmobj_t *o);
static int  shadow_lookuppage(mmobj_t *o, uint32_t pagenum, int forwrite, pframe_t **pf);
static int  shadow_fillpage(mmobj_t *o, pframe_t *pf);
static int  shadow_dirtypage(mmobj_t *o, pframe_t *pf);
static int  shadow_cleanpage(mmobj_t *o, pframe_t *pf);

static mmobj_ops_t shadow_mmobj_ops = {
        .ref = shadow_ref,
        .put = shadow_put,
        .lookuppage = shadow_lookuppage,
        .fillpage  = shadow_fillpage,
        .dirtypage = shadow_dirtypage,
        .cleanpage = shadow_cleanpage
};

/*
 * This function is called at boot time to initialize the
 * shadow page sub system. Currently it only initializes the
 * shadow_allocator object.
 */
void
shadow_init()
{
        /*NOT_YET_IMPLEMENTED("VM: shadow_init");*/
	dbg(DBG_CORE,"Enter shadow_init\n");
	shadow_allocator = slab_allocator_create("shadow", sizeof(mmobj_t));
	
	KASSERT(shadow_allocator);
	dbg(DBG_PRINT,"(GRADING3A 6.a)\n");	
	
	dbg(DBG_CORE,"Exit shadow_init\n");

}

/*
 * You'll want to use the shadow_allocator to allocate the mmobj to
 * return, then then initialize it. Take a look in mm/mmobj.h for
 * macros or functions which can be of use here. Make sure your initial
 * reference count is correct.
 */
mmobj_t *
shadow_create()
{
        /*NOT_YET_IMPLEMENTED("VM: shadow_create");
        return NULL;*/
	/*dbg(DBG_PRINT,"(GRADING3D 1) shadow_create() is called from vfstest\n");*/
	 mmobj_t *obj = slab_obj_alloc(shadow_allocator);

    	if (obj){
		mmobj_init(obj, &shadow_mmobj_ops);
		obj->mmo_un.mmo_bottom_obj=mmobj_bottom_obj(obj);
                obj->mmo_refcount=1;
		dbg(DBG_PRINT,"(GRADING3D 1) when shadow_allocator is success to initialize the obj\n");
    	}
	dbg(DBG_PRINT,"(GRADING3D 1) return of mmobj in shadow_create()\n");
    	return obj;
}

/* Implementation of mmobj entry points: */

/*
 * Increment the reference count on the object.
 */
static void
shadow_ref(mmobj_t *o)
{
        /*NOT_YET_IMPLEMENTED("VM: shadow_ref");*/
	dbg(DBG_CORE,"Enter shadow_init\n");

	KASSERT(o && (0 < o->mmo_refcount) && (&shadow_mmobj_ops == o->mmo_ops));
	dbg(DBG_PRINT,"(GRADING3A 6.b)\n");	
	
	o->mmo_refcount++;
	dbg(DBG_PRINT,"(GRADING3D 1) return success from shadow_init\n");
}

/*
 * Decrement the reference count on the object. If, however, the
 * reference count on the object reaches the number of resident
 * pages of the object, we can conclude that the object is no
 * longer in use and, since it is a shadow object, it will never
 * be used again. You should unpin and uncache all of the object's
 * pages and then free the object itself.
 */
static void
shadow_put(mmobj_t *o)
{
        /*NOT_YET_IMPLEMENTED("VM: shadow_put");*/
	

	dbg(DBG_CORE,"Entering shadow_put()\n");
	KASSERT(o && (0 < o->mmo_refcount) && (&shadow_mmobj_ops == o->mmo_ops));
	dbg(DBG_PRINT,"(GRADING3A 6.c)\n");
	

	 if (o->mmo_refcount -1 == o->mmo_nrespages )
	{
		 pframe_t *p;
        	list_iterate_begin(&o->mmo_respages, p, pframe_t, pf_olink)
		{
			/* never reached */
			/*while(pframe_is_busy(p))
			{
				sched_sleep_on(&p->pf_waitq);
				dbg(DBG_PRINT,"(GRADING3D 1) when pframe is busy to sleep shadow_put() \n"); 
			}*/
	             while(pframe_is_pinned(p))
                        {
				pframe_unpin(p);
				dbg(DBG_PRINT,"(GRADING3D 1) when pframe is pinned to unpin shadow_put() \n");
			}
		    if(pframe_is_dirty(p))
			{
				pframe_clean(p);
				dbg(DBG_PRINT,"(GRADING3D 1) when pframe is dirty to clean in shadow_put() \n");
			}
			pframe_free(p);
			dbg(DBG_PRINT,"(GRADING3D 1) free pframe in shadow_put() \n");
	        } 
		list_iterate_end();

        	o->mmo_shadowed->mmo_ops->put(o->mmo_shadowed);
        	slab_obj_free(shadow_allocator,o);
		dbg(DBG_PRINT,"(GRADING3D 1) free allocator in shadow_put() \n");
	}
	
	o->mmo_refcount--;
	dbg(DBG_PRINT,"(GRADING3D 1) return success from shadow_put() \n");
}

/* This function looks up the given page in this shadow object. The
 * forwrite argument is true if the page is being looked up for
 * writing, false if it is being looked up for reading. This function
 * must handle all do-not-copy-on-not-write magic (i.e. when forwrite
 * is false find the first shadow object in the chain which has the
 * given page resident). copy-on-write magic (necessary when forwrite
 * is true) is handled in shadow_fillpage, not here. It is important to
 * use iteration rather than recursion here as a recursive implementation
 * can overflow the kernel stack when looking down a long shadow chain */
static int
shadow_lookuppage(mmobj_t *o, uint32_t pagenum, int forwrite, pframe_t **pf)
{
        /*NOT_YET_IMPLEMENTED("VM: shadow_lookuppage");
        return 0;*/

	 if (forwrite){
	dbg(DBG_PRINT,"(GRADING3D 1) from memtest when forwrite is 1 in shadow_lookuppage() \n");
        return pframe_get(o, pagenum, pf);
    }

    pframe_t *p;
    mmobj_t *curr = o;

    while (p == NULL && curr->mmo_shadowed != NULL){
        p = pframe_get_resident(curr, pagenum);
	if(p==NULL)
	{
        	curr = curr->mmo_shadowed;
		dbg(DBG_PRINT,"(GRADING3D 1) when pf=null in shadow_lookuppage() \n");
	}
	else
	{	
		*pf=p;
		dbg(DBG_PRINT,"(GRADING3D 1) when p is not null in shadow_lookuppage() \n");
		return 0;
	}
    }

        int res = pframe_lookup(curr, pagenum, 0, &p);
	if(res == 0)	
	{
		*pf = p;
                KASSERT(NULL != (*pf));
                KASSERT((pagenum == (*pf)->pf_pagenum) && (!pframe_is_busy(*pf)));
                dbg(DBG_PRINT,"(GRADING3A 6.d)\n");
		/*dbg(DBG_PRINT,"(GRADING3D 2) return success in shadow_lookuppage() \n");*/
		return res;
	}
       dbg(DBG_PRINT,"(GRADING3D 2) return failure in shadow_lookuppage() \n");
    	return res;
}

/* As per the specification in mmobj.h, fill the page frame starting
 * at address pf->pf_addr with the contents of the page identified by
 * pf->pf_obj and pf->pf_pagenum. This function handles all
 * copy-on-write magic (i.e. if there is a shadow object which has
 * data for the pf->pf_pagenum-th page then we should take that data,
 * if no such shadow object exists we need to follow the chain of
 * shadow objects all the way to the bottom object and take the data
 * for the pf->pf_pagenum-th page from the last object in the chain).
 * It is important to use iteration rather than recursion here as a 
 * recursive implementation can overflow the kernel stack when 
 * looking down a long shadow chain */
static int
shadow_fillpage(mmobj_t *o, pframe_t *pf)
{
        /*NOT_YET_IMPLEMENTED("VM: shadow_fillpage");
        return 0;*/
	
	KASSERT(pframe_is_busy(pf));
        KASSERT(!pframe_is_pinned(pf));
	dbg(DBG_PRINT,"(GRADING3A 6.e)\n");


	pframe_pin(pf);

	 pframe_t *p ;
	int lookup_res = pframe_lookup(o->mmo_shadowed, pf->pf_pagenum, 1, &p);
	if(lookup_res ==0)
	{
		memcpy(pf->pf_addr, p->pf_addr, PAGE_SIZE);
		dbg(DBG_PRINT,"(GRADING3D 1) return success in shadow_fillpage() \n");
		return 0;
	}
   	else
	dbg(DBG_PRINT,"(GRADING3D 2) return error in shadow_fillpage() \n"); 
	return lookup_res;
}

/* These next two functions are not difficult. */

static int
shadow_dirtypage(mmobj_t *o, pframe_t *pf)
{
        /*NOT_YET_IMPLEMENTED("VM: shadow_dirtypage");
        return -1;*/
	dbg(DBG_PRINT,"(GRADING3D 1) shadow_dirtypage() is called from vfstest\n");
	pframe_set_dirty(pf);
	return 0;
}

static int
shadow_cleanpage(mmobj_t *o, pframe_t *pf)
{
        /*NOT_YET_IMPLEMENTED("VM: shadow_cleanpage");
        return -1;*/
	dbg(DBG_PRINT,"(GRADING3D 1) shadow_cleanpage() is called from vfstest\n");
	pframe_clear_dirty(pf);
	return 0;
}
