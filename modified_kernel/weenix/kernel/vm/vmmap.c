/******************************************************************************/
/* Important Summer 2017 CSCI 402 usage information:                          */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "kernel.h"
#include "errno.h"
#include "globals.h"

#include "vm/vmmap.h"
#include "vm/shadow.h"
#include "vm/anon.h"

#include "proc/proc.h"

#include "util/debug.h"
#include "util/list.h"
#include "util/string.h"
#include "util/printf.h"

#include "fs/vnode.h"
#include "fs/file.h"
#include "fs/fcntl.h"
#include "fs/vfs_syscall.h"

#include "mm/slab.h"
#include "mm/page.h"
#include "mm/mm.h"
#include "mm/mman.h"
#include "mm/mmobj.h"

static slab_allocator_t *vmmap_allocator;
static slab_allocator_t *vmarea_allocator;

void
vmmap_init(void)
{
        vmmap_allocator = slab_allocator_create("vmmap", sizeof(vmmap_t));
        KASSERT(NULL != vmmap_allocator && "failed to create vmmap allocator!");
        vmarea_allocator = slab_allocator_create("vmarea", sizeof(vmarea_t));
        KASSERT(NULL != vmarea_allocator && "failed to create vmarea allocator!");
}

vmarea_t *
vmarea_alloc(void)
{
        vmarea_t *newvma = (vmarea_t *) slab_obj_alloc(vmarea_allocator);
        if (newvma) {
                newvma->vma_vmmap = NULL;
        }
        return newvma;
}

void
vmarea_free(vmarea_t *vma)
{
        KASSERT(NULL != vma);
        slab_obj_free(vmarea_allocator, vma);
}

/* a debugging routine: dumps the mappings of the given address space. */
size_t
vmmap_mapping_info(const void *vmmap, char *buf, size_t osize)
{
        KASSERT(0 < osize);
        KASSERT(NULL != buf);
        KASSERT(NULL != vmmap);

        vmmap_t *map = (vmmap_t *)vmmap;
        vmarea_t *vma;
        ssize_t size = (ssize_t)osize;

        int len = snprintf(buf, size, "%21s %5s %7s %8s %10s %12s\n",
                           "VADDR RANGE", "PROT", "FLAGS", "MMOBJ", "OFFSET",
                           "VFN RANGE");

        list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {
                size -= len;
                buf += len;
                if (0 >= size) {
                        goto end;
                }

                len = snprintf(buf, size,
                               "%#.8x-%#.8x  %c%c%c  %7s 0x%p %#.5x %#.5x-%#.5x\n",
                               vma->vma_start << PAGE_SHIFT,
                               vma->vma_end << PAGE_SHIFT,
                               (vma->vma_prot & PROT_READ ? 'r' : '-'),
                               (vma->vma_prot & PROT_WRITE ? 'w' : '-'),
                               (vma->vma_prot & PROT_EXEC ? 'x' : '-'),
                               (vma->vma_flags & MAP_SHARED ? " SHARED" : "PRIVATE"),
                               vma->vma_obj, vma->vma_off, vma->vma_start, vma->vma_end);
        } list_iterate_end();

end:
        if (size <= 0) {
                size = osize;
                buf[osize - 1] = '\0';
        }
        /*
        KASSERT(0 <= size);
        if (0 == size) {
                size++;
                buf--;
                buf[0] = '\0';
        }
        */
        return osize - size;
}

/* Create a new vmmap, which has no vmareas and does
 * not refer to a process. */
vmmap_t *
vmmap_create(void)
{
       /* NOT_YET_IMPLEMENTED("VM: vmmap_create");
        return NULL;*/
	dbg(DBG_CORE,"Entering vmmap_create\n");
	vmmap_t* map = (vmmap_t *)slab_obj_alloc(vmmap_allocator);
	map->vmm_proc = NULL;
	list_init(&map->vmm_list);
	dbg(DBG_PRINT,"(GRADING3D 2) return sucess from vmmap_create\n");
	return map;
}

/* Removes all vmareas from the address space and frees the
 * vmmap struct. */
void
vmmap_destroy(vmmap_t *map)
{
       /* NOT_YET_IMPLEMENTED("VM: vmmap_destroy");*/
	KASSERT(NULL != map);
	dbg(DBG_PRINT,"(GRADING3A 3.a)\n");

	dbg(DBG_CORE,"Entering vmmap_destroy\n");
	if(!list_empty(&map->vmm_list)) {
		
                vmarea_t *vmarea;
                list_iterate_begin(&map->vmm_list, vmarea, vmarea_t, vma_plink) {  
                        vmarea->vma_obj->mmo_ops->put(vmarea->vma_obj);
        		list_remove(&vmarea->vma_plink);
       			 list_remove(&vmarea->vma_olink);
        		vmarea_free(vmarea);
                } list_iterate_end();
		dbg(DBG_PRINT,"(GRADING3D 2) if list (map->vmm_list) is not empty\n");
        }
	slab_obj_free(vmmap_allocator, map);
	dbg(DBG_PRINT,"(GRADING3D 2) return from vmmap_destroy\n");
			
}

/* Add a vmarea to an address space. Assumes (i.e. asserts to some extent)
 * the vmarea is valid.  This involves finding where to put it in the list
 * of VM areas, and adding it. Don't forget to set the vma_vmmap for the
 * area. */
void
vmmap_insert(vmmap_t *map, vmarea_t *newvma)
{
        /*NOT_YET_IMPLEMENTED("VM: vmmap_insert");*/
	KASSERT(NULL != map && NULL != newvma);
	KASSERT(NULL == newvma->vma_vmmap);
	KASSERT(newvma->vma_start < newvma->vma_end);
	KASSERT(ADDR_TO_PN(USER_MEM_LOW) <= newvma->vma_start && ADDR_TO_PN(USER_MEM_HIGH) >= newvma->vma_end);
	dbg(DBG_PRINT,"(GRADING3A 3.b)\n");

	newvma->vma_vmmap = map;
		
	if(!list_empty(&map->vmm_list))
	{
		vmarea_t* vmarea;
		list_iterate_begin(&map->vmm_list, vmarea, vmarea_t, vma_plink)
		{
			if(vmarea->vma_start > newvma->vma_start)
			{
				list_insert_before(&vmarea->vma_plink,&newvma->vma_plink);
				dbg(DBG_PRINT,"(GRADING3D 2) return from vmmap_insert\n");
				return;
			}
		}list_iterate_end();
		list_insert_tail(&map->vmm_list, &newvma->vma_plink);
		dbg(DBG_PRINT,"(GRADING3D 2) if list (map->vmm_list) is not empty\n");
	}
	else
	{
		list_insert_head(&map->vmm_list,&newvma->vma_plink);
		dbg(DBG_PRINT,"(GRADING3D 2) return from vmmap_insert\n");
		return;
	}
}

/* Find a contiguous range of free virtual pages of length npages in
 * the given address space. Returns starting vfn for the range,
 * without altering the map. Returns -1 if no such range exists.
 *
 * Your algorithm should be first fit. If dir is VMMAP_DIR_HILO, you
 * should find a gap as high in the address space as possible; if dir
 * is VMMAP_DIR_LOHI, the gap should be as low as possible. */
int
vmmap_find_range(vmmap_t *map, uint32_t npages, int dir)
{
        /*NOT_YET_IMPLEMENTED("VM: vmmap_find_range");
        return -1;*/
	/*dbg(DBG_PRINT,"(GRADING3D 2) vmmap_find_range is called from vfstest\n");*/
	if(dir == VMMAP_DIR_HILO)
	{
		uint32_t vfn = ADDR_TO_PN(USER_MEM_HIGH);
		vmarea_t* vma;
		list_iterate_reverse(&map->vmm_list, vma, vmarea_t, vma_plink)
		{
			if((vfn - vma->vma_end) >= npages)
			{
				dbg(DBG_PRINT,"(GRADING3D 2) return from vmmap_find_range\n");
				return (vfn - npages);
			}
			vfn = vma->vma_start;
		}list_iterate_end();
		
		if((vfn - ADDR_TO_PN(USER_MEM_LOW)) >= npages)
		{
			dbg(DBG_PRINT,"(GRADING3D 2) return from vmmap_find_range when vfn - ADDR_TO_PN(USER_MEM_LOW)) >= npages\n"); 
			return (vfn-npages);
		}
		dbg(DBG_PRINT,"(GRADING3D 2) dir = VMMAP_DIR_HILO\n");
	}
	/* never reached */
	/*else if(dir == VMMAP_DIR_LOHI)
	{
		uint32_t vfn = ADDR_TO_PN(USER_MEM_LOW);
		vmarea_t* vma;
		list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink)
		{
			if((vma->vma_start - vfn) >= npages)
			{
				dbg(DBG_PRINT,"(GRADING3D 1) return from vmmap_find_range when (vma->vma_start - vfn) >= npages\n");
				return vfn;
			}
			vfn = vma->vma_end;
		}list_iterate_end();
		if((ADDR_TO_PN(USER_MEM_HIGH) - vfn) >= npages)
		{
			dbg(DBG_PRINT,"(GRADING3D 1) return from vmmap_find_range when (ADDR_TO_PN(USER_MEM_HIGH) - vfn) >= npages\n");
			return vfn;
		}
		dbg(DBG_PRINT,"(GRADING3D 2) dir = VMMAP_DIR_LOHI\n");
	}*/
	dbg(DBG_PRINT,"(GRADING3D 2) return failure from vmmap_find_range\n");
	return -1;	
}

/* Find the vm_area that vfn lies in. Simply scan the address space
 * looking for a vma whose range covers vfn. If the page is unmapped,
 * return NULL. */
vmarea_t *
vmmap_lookup(vmmap_t *map, uint32_t vfn)
{
       /* NOT_YET_IMPLEMENTED("VM: vmmap_lookup");
        return NULL;*/
	dbg(DBG_CORE,"Enter vmmap_lookup\n");

	KASSERT(NULL != map);
	dbg(DBG_PRINT,"(GRADING3A 3.c)\n");

	if(!list_empty(&map->vmm_list)) 
        {
		vmarea_t* vmarea;
		list_iterate_begin(&map->vmm_list,vmarea,vmarea_t,vma_plink)
		{
			if(vmarea->vma_start <= vfn && vmarea->vma_end > vfn)
			{
				dbg(DBG_PRINT,"(GRADING3D 2) return from vmmap_lookup\n");
				return vmarea;
			}
		}list_iterate_end();
		dbg(DBG_PRINT,"(GRADING3D 2) if list (map->vmm_list) is not empty\n");
	}	
	dbg(DBG_PRINT,"(GRADING3D 2) return failure from vmmap_lookup\n");
	return NULL;
}

/* Allocates a new vmmap containing a new vmarea for each area in the
 * given map. The areas should have no mmobjs set yet. Returns pointer
 * to the new vmmap on success, NULL on failure. This function is
 * called when implementing fork(2). */
vmmap_t *
vmmap_clone(vmmap_t *map)
{
        /*NOT_YET_IMPLEMENTED("VM: vmmap_clone");
        return NULL;*/
	
	/*dbg(DBG_PRINT,"(GRADING3D 2) vmmap_clone enter\n");*/
	
	vmmap_t* newmap = vmmap_create();
	if(newmap)
	{
		vmarea_t* newarea;
		vmarea_t* child;

		list_iterate_begin(&map->vmm_list, child, vmarea_t, vma_plink)
		{
        		newarea=vmarea_alloc();
			/* never reached*/
        	        /*if(newarea == NULL)
			{
				dbg(DBG_PRINT,"(GRADING3D 1) new vmarea allocation failed in vmmap_clone\n");
				return NULL;
			}
			else*/
			if(newarea != NULL)
			{
        	        	newarea->vma_start=child->vma_start;
        	        	newarea->vma_end=child->vma_end;
        	        	newarea->vma_prot=child->vma_prot;
        	        	newarea->vma_flags=child->vma_flags;
				newarea->vma_off=child->vma_off;
				newarea->vma_obj = NULL;
				newarea->vma_vmmap = NULL;
				list_init(&newarea->vma_plink);
				list_init(&newarea->vma_olink);
                		vmmap_insert(newmap,newarea);
				dbg(DBG_PRINT,"(GRADING3D 1) initialization of vmarea in vmmap_clone\n");
			}
	        }list_iterate_end();
		dbg(DBG_PRINT,"(GRADING3D 1) return success from vmmap_clone\n");
		return newmap;
	}
	/* never reached*/
	else
	{
		dbg(DBG_PRINT,"(GRADING3D 1) return failure from vmmap_clone\n");
		return NULL;
	}
}

/* Insert a mapping into the map starting at lopage for npages pages.
 * If lopage is zero, we will find a range of virtual addresses in the
 * process that is big enough, by using vmmap_find_range with the same
 * dir argument.  If lopage is non-zero and the specified region
 * contains another mapping that mapping should be unmapped.
 *
 * If file is NULL an anon mmobj will be used to create a mapping
 * of 0's.  If file is non-null that vnode's file will be mapped in
 * for the given range.  Use the vnode's mmap operation to get the
 * mmobj for the file; do not assume it is file->vn_obj. Make sure all
 * of the area's fields except for vma_obj have been set before
 * calling mmap.
 *
 * If MAP_PRIVATE is specified set up a shadow object for the mmobj.
 *
 * All of the input to this function should be valid (KASSERT!).
 * See mmap(2) for for description of legal input.
 * Note that off should be page aligned.
 *
 * Be very careful about the order operations are performed in here. Some
 * operation are impossible to undo and should be saved until there
 * is no chance of failure.
 *
 * If 'new' is non-NULL a pointer to the new vmarea_t should be stored in it.
 */
int
vmmap_map(vmmap_t *map, vnode_t *file, uint32_t lopage, uint32_t npages,
          int prot, int flags, off_t off, int dir, vmarea_t **new)
{
        /*NOT_YET_IMPLEMENTED("VM: vmmap_map");
        return -1;*/
	/*dbg(DBG_PRINT,"(GRADING3D 1) in vmmap_map\n");*/

	KASSERT(NULL != map);
	KASSERT(0 < npages);
	KASSERT((MAP_SHARED & flags) || (MAP_PRIVATE & flags));
	KASSERT((0 == lopage) || (ADDR_TO_PN(USER_MEM_LOW) <= lopage));
	KASSERT((0 == lopage) || (ADDR_TO_PN(USER_MEM_HIGH) >= (lopage + npages)));
	KASSERT(PAGE_ALIGNED(off));
	dbg(DBG_PRINT, "(GRADING3A 3.d) \n");
	
	vmarea_t* newvma = vmarea_alloc();
	newvma->vma_obj = NULL;
	newvma->vma_vmmap = NULL;
	newvma->vma_start = lopage;
	newvma->vma_end = lopage + npages;
	newvma->vma_off = off/PAGE_SIZE;
	newvma->vma_prot = prot;
	newvma->vma_flags = flags;
	list_init(&newvma->vma_plink);
	list_init(&newvma->vma_olink);
	
	if(lopage == 0)
	{
		int range = vmmap_find_range(map, npages, dir);
		if(range < 0)
		{
			dbg(DBG_PRINT,"(GRADING3D 2) if range < 0 vmmap_map\n");
			return range;
		}
		else
		{
			lopage = range;
			newvma->vma_start = lopage;
			newvma->vma_end = lopage + npages;
			dbg(DBG_PRINT,"(GRADING3D 2) if range >= 0 vmmap_mp\n");
		}
	}
	
		int res = vmmap_is_range_empty(map, lopage, npages);
		if(res == 0)
		{
			vmmap_remove(map, lopage, npages);
			dbg(DBG_PRINT,"(GRADING3D 2) range empty failure vmmap_map\n");
		}
	
	
	if(file == NULL)
	{
		newvma->vma_obj = anon_create();
		dbg(DBG_PRINT,"(GRADING3D 2) if file is NULL\n");
	}
	else
	{
		file->vn_ops->mmap(file, newvma, &newvma->vma_obj);
		dbg(DBG_PRINT,"(GRADING3D 2) if file is not NULL\n");
	}
	
	list_insert_head(&((newvma->vma_obj->mmo_un).mmo_vmas), &newvma->vma_olink);
	
	if((flags&MAP_PRIVATE) == MAP_PRIVATE)
	{
		mmobj_t* obj = shadow_create();
		obj->mmo_un.mmo_bottom_obj = newvma->vma_obj;
		obj->mmo_shadowed = newvma->vma_obj;
		newvma->vma_obj = obj;
		dbg(DBG_PRINT,"(GRADING3D 2) if flags = MAP_PRIVATE\n");
	}
	
	vmmap_insert(map, newvma);
	
	if(new != NULL)
	{
		*new = newvma;
		dbg(DBG_PRINT,"(GRADING3D 2) new is not NULL\n");
	}
	
	dbg(DBG_PRINT,"(GRADING3D 2) return success from vmmap_map\n");
	return 0;
}

/*
 * We have no guarantee that the region of the address space being
 * unmapped will play nicely with our list of vmareas.
 *
 * You must iterate over each vmarea that is partially or wholly covered
 * by the address range [addr ... addr+len). The vm-area will fall into one
 * of four cases, as illustrated below:
 *
 * key:
 *          [             ]   Existing VM Area
 *        *******             Region to be unmapped
 *
 * Case 1:  [   ******    ]
 * The region to be unmapped lies completely inside the vmarea. We need to
 * split the old vmarea into two vmareas. be sure to increment the
 * reference count to the file associated with the vmarea.
 *
 * Case 2:  [      *******]**
 * The region overlaps the end of the vmarea. Just shorten the length of
 * the mapping.
 *
 * Case 3: *[*****        ]
 * The region overlaps the beginning of the vmarea. Move the beginning of
 * the mapping (remember to update vma_off), and shorten its length.
 *
 * Case 4: *[*************]**
 * The region completely contains the vmarea. Remove the vmarea from the
 * list.
 */
int
vmmap_remove(vmmap_t *map, uint32_t lopage, uint32_t npages)
{
        /*NOT_YET_IMPLEMENTED("VM: vmmap_remove");
        return -1;*/
	
	uint32_t vfn_start = lopage;
	uint32_t vfn_end = lopage + npages;
	uint32_t i;
	for(i = vfn_start; i < vfn_end; i++)
	{
		vmarea_t* vma = vmmap_lookup(map, i);
		if(vma)
		{
			uint32_t temp = vma->vma_end;
			if( (vma->vma_start >= i) && (vma->vma_end <= vfn_end) )
			{
				/* Case 4: *[*************]** */
				
				vma->vma_obj->mmo_ops->put(vma->vma_obj);
				list_remove(&vma->vma_plink);
				list_remove(&vma->vma_olink);
				
				vmarea_free(vma);
				dbg(DBG_PRINT,"(GRADING3D 2) case 4 in vmmap_remove\n");
			}
			else if( (vma->vma_start >= i) && (vma->vma_end > vfn_end) )
			{
				/* Case 3: *[*****        ] */
				
				vma->vma_off = vma->vma_off + vfn_end - vma->vma_start;
				vma->vma_start = vfn_end;
				dbg(DBG_PRINT,"(GRADING3D 2) case 3 in vmmap_remove\n");
			}
			else if( (vma->vma_start < i) && (vma->vma_end <= vfn_end) )
			{
				/* Case 2:  [      *******]** */
				
				vma->vma_end = i;
				dbg(DBG_PRINT,"(GRADING3D 2) case 2 in vmmap_remove\n");
			}
			else if( (vma->vma_start < i) && (vma->vma_end > vfn_end) )
			{
				/* Case 1:  [   ******    ] */
				
				vmarea_t* nvma = vmarea_alloc();
				nvma->vma_start = vfn_end;
				nvma->vma_end = vma->vma_end;
				nvma->vma_off = vma->vma_off + (vfn_end - vma->vma_start);
				nvma->vma_prot = vma->vma_prot;
				nvma->vma_flags = vma->vma_flags;
				nvma->vma_vmmap = NULL;
				list_init(&nvma->vma_plink);
				list_init(&nvma->vma_olink);
				
				vma->vma_end = i;
				vmmap_insert(map, nvma);
				
				if((vma->vma_flags&MAP_PRIVATE) == MAP_PRIVATE)
				{
					mmobj_t* obj1 = mmobj_bottom_obj(vma->vma_obj);
					mmobj_t* obj2 = vma->vma_obj;
					mmobj_t* sobj1 = shadow_create();
					mmobj_t* sobj2 = shadow_create();
					sobj1->mmo_shadowed = obj2;
					sobj2->mmo_shadowed = obj2;
					sobj1->mmo_un.mmo_bottom_obj = obj1;
					sobj2->mmo_un.mmo_bottom_obj = obj1;
					vma->vma_obj = sobj1;
					nvma->vma_obj = sobj2;
					obj2->mmo_ops->ref(obj2);
					list_insert_head(&obj1->mmo_un.mmo_vmas, &nvma->vma_olink);
					dbg(DBG_PRINT,"(GRADING3D 2) shadow object creation for case 1 in vmmap_remove\n");
				}
				dbg(DBG_PRINT,"(GRADING3D 2) case 1 in vmmap_remove\n");
			}
			i = temp - 1;
			dbg(DBG_PRINT,"(GRADING3D 2) if vma_lookup does not return NULL\n");
		}
	}
	pt_unmap_range( curproc->p_pagedir, (uint32_t)PN_TO_ADDR(lopage), ((uint32_t)PN_TO_ADDR(lopage)+(uintptr_t)(PAGE_SIZE*npages)) );
	dbg(DBG_PRINT,"(GRADING3D 2) return success from vmmap_remove\n");
	return 0;
}

/*
 * Returns 1 if the given address space has no mappings for the
 * given range, 0 otherwise.
 */
int
vmmap_is_range_empty(vmmap_t *map, uint32_t startvfn, uint32_t npages)
{
        /*NOT_YET_IMPLEMENTED("VM: vmmap_is_range_empty");
        return 0;*/
	dbg(DBG_CORE,"Entering vmmap_is_range_empty\n");
	KASSERT((startvfn < startvfn+npages) && (ADDR_TO_PN(USER_MEM_LOW) <= startvfn) && (ADDR_TO_PN(USER_MEM_HIGH) >= startvfn+npages));
	dbg(DBG_PRINT,"(GRADING3A 3.e)\n");

	if(!list_empty(&map->vmm_list))
	{
		vmarea_t *vmarea;
		uint32_t end = startvfn+npages;
		list_iterate_begin(&map->vmm_list, vmarea, vmarea_t, vma_plink) 
		{
		if((vmarea->vma_start<startvfn && vmarea->vma_end > end) || (vmarea->vma_start<startvfn && vmarea->vma_end <= end && startvfn<vmarea->vma_end) || (vmarea->vma_start>=startvfn && vmarea->vma_end <= end) || (vmarea->vma_start >= startvfn && vmarea->vma_end > end && vmarea->vma_start < end))
 		{	
			dbg(DBG_PRINT,"(GRADING3D 2) return from vmmap_is_range_empty\n");
			return 0;
		}
		}list_iterate_end();
		dbg(DBG_PRINT,"(GRADING3D 2) if list (map->vmm_list) is not empty\n");
	}
	
		dbg(DBG_PRINT,"(GRADING3D 2) return failure from vmmap_is_range_empty\n");
		return 1; 
	
}

/* Read into 'buf' from the virtual address space of 'map' starting at
 * 'vaddr' for size 'count'. To do so, you will want to find the vmareas
 * to read from, then find the pframes within those vmareas corresponding
 * to the virtual addresses you want to read, and then read from the
 * physical memory that pframe points to. You should not check permissions
 * of the areas. Assume (KASSERT) that all the areas you are accessing exist.
 * Returns 0 on success, -errno on error.
 */
int
vmmap_read(vmmap_t *map, const void *vaddr, void *buf, size_t count)
{
       /* NOT_YET_IMPLEMENTED("VM: vmmap_read");
        return 0;*/
	dbg(DBG_CORE,"Entering vmmap_read\n");
	vmarea_t *vmarea;
	uint32_t uaddr = (uint32_t)vaddr;
	uint32_t offset;
	size_t data_read;
	int page_number;
	uint32_t vfn;
	size_t to_read = count;
	char *buffer = (char *)buf;
	pframe_t *pframe;
	
	while(to_read > 0){
 	pframe_t *pframe;
 	
	vfn = ADDR_TO_PN(uaddr);
	vmarea = vmmap_lookup(map,vfn);
	page_number = vmarea->vma_off + (vfn - (vmarea->vma_start));
	pframe_lookup(vmarea->vma_obj,page_number,0,&pframe);
	offset = PAGE_OFFSET(uaddr);
	data_read = MIN((PAGE_SIZE-offset),to_read);
	
	memcpy(buffer,(char *)pframe->pf_addr+offset,data_read);
	to_read = to_read - data_read;
	buffer = buffer + data_read;
	uaddr = uaddr + data_read;	
	dbg(DBG_PRINT,"(GRADING3D 2) in vmmap_read\n");
	}
	dbg(DBG_PRINT,"(GRADING3D 2) return from vmmap_read\n");
	return 0;	
}

/* Write from 'buf' into the virtual address space of 'map' starting at
 * 'vaddr' for size 'count'. To do this, you will need to find the correct
 * vmareas to write into, then find the correct pframes within those vmareas,
 * and finally write into the physical addresses that those pframes correspond
 * to. You should not check permissions of the areas you use. Assume (KASSERT)
 * that all the areas you are accessing exist. Remember to dirty pages!
 * Returns 0 on success, -errno on error.
 */
int
vmmap_write(vmmap_t *map, void *vaddr, const void *buf, size_t count)
{
        /*NOT_YET_IMPLEMENTED("VM: vmmap_write");
        return 0;*/
	dbg(DBG_CORE,"Entering vmmap_write\n");
	vmarea_t *vmarea;
        uint32_t uaddr = (uint32_t)vaddr;
        uint32_t offset;
        size_t data_write;
        int page_number;
        uint32_t vfn;
	size_t to_write = count;
	char *buffer = (char *)buf;
	pframe_t *pframe;
	
	while(to_write > 0){
        pframe_t *pframe;
		
	vfn = ADDR_TO_PN(uaddr);
        vmarea = vmmap_lookup(map,vfn);
        page_number = vmarea->vma_off + (vfn - (vmarea->vma_start));
        pframe_lookup(vmarea->vma_obj,page_number,1,&pframe);
        offset = PAGE_OFFSET(uaddr);
	data_write = MIN((PAGE_SIZE-offset),to_write); 
        
        memcpy((char *)pframe->pf_addr+offset,buffer,data_write);
	to_write = to_write - data_write;
	buffer = buffer + data_write;
	uaddr = uaddr + data_write;
	pframe_dirty(pframe);
	dbg(DBG_PRINT,"(GRADING3D 2) in vmmap_write\n");
	}
	dbg(DBG_PRINT,"(GRADING3D 2) return from vmmap_write\n");
	return 0;
}
