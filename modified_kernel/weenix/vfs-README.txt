Documentation for Kernel Assignment 2
=====================================

+------------------------+
| BUILD & RUN (Required) |
+------------------------+

Replace "(Comments?)" with the command the grader should use to compile
    your kernel (it should simply be "make").
    To compile the kernel, the grader should type: make

+--------------------+
| GRADING (Required) |
+--------------------+

(A.1) In fs/vnode.c:
    (a) In special_file_read():  6 out of 6 pts
    (b) In special_file_write():  6 out of 6 pts

(A.2) In fs/namev.c:
    (a) In lookup():  6 out of 6 pts
    (b) In dir_namev(): 10 out of 10 pts
    (c) In open_namev(): 2 out of 2 pts

(A.3) In fs/vfs_syscall.c:
    (a) In do_write(): 6 out of 6 pts
    (b) In do_mknod(): 2 out of 2 pts
    (c) In do_mkdir(): 2 out of 2 pts
    (d) In do_rmdir(): 2 out of 2 pts
    (e) In do_unlink(): 2 out of 2 pts
    (f) In do_stat(): 2 out of 2 pts

(B) vfstest: 39 out of 39 pts
    What is your kshell command to invoke vfstest_main(): vfstest

(C.1) faber_fs_thread_test (3 out of 3 pts)
    What is your kshell command to invoke faber_fs_thread_test(): thrtest n (where n is 2,3,4)
(C.2) faber_directory_test (2 out of 2 pts)
    What is your kshell command to invoke faber_directory_test(): dirtest n (where n is 10,20,30,40)

(D) Self-checks: (10 out of 10 pts)
    Comments: Done

Missing/incomplete required section(s) in README file (vfs-README.txt): (-0 pts)
Submitted binary file : (-0 pts)
Submitted extra (unmodified) file : (-0 pts)
Wrong file location in submission : (-0 pts)
Extra printout DBG=error,test tout is for which item in the grading guidelines : (-0 pts)
Incorrectly formatted or mis-labeled "conforming dbg() calls" : (-0 pts)
Cannot compile : (-0 pts)
Compiler warnings : (-0 pts)
"make clean" : (-0 pts)
Kernel panic : (-0 pts)
Kernel hangs : (-0 pts)
Cannot halt kernel cleanly : (-0 pts)

+---------------------------------+
| BUGS / TESTS TO SKIP (Required) |
+---------------------------------+

Is there are any tests in the standard test suite that you know that it's not
working and you don't want the grader to run it at all so you won't get extra
deductions, please list them here.  (Of course, if the grader won't run these
tests, you will not get plus points for them.)  If there is none, please replace
the question mark with "none".

Comments: None

+--------------------------------------+
| CONTRIBUTION FROM MEMBERS (Required) |
+--------------------------------------+

1)  Names and USC e-mail addresses of team members: 
	a) Adeesh Nagpal                -> anagpal@usc.edu
        b) Oliver Edward Day            -> oliver.day@usc.edu
        c) Monika Devanga Ravi          -> devangar@usc.edu
        d) Prajwal Gandige Sangamesh    -> gandiges@usc.edu

2)  Is the following statement correct about your submission (please replace
        "(Comments?)" with either "yes" or "no", and if the answer is "no",
        please list percentages for each team member)  "Each team member
        contributed equally in this assignment": Yes.

+------------------+
| OTHER (Optional) |
+------------------+

Comments on deviation from spec (you will still lose points, but it's better to let the grader know): Followed the Kernel-2 spec.
General comments on design decisions: Followed the Kernel-2 spec. 
